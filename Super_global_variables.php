<?php

$x = 75;

function doSomething(){
    $x = 25;
    echo "local x = $x ";
    echo "<br>";

    echo "global x = " . $GLOBALS['x'];
}

doSomething();

echo $_SERVER['PHP_SELF'] . "<br>";

echo $_SERVER['SERVER_NAME'] . "<br>";

echo $_SERVER['SCRIPT_FILENAME'] . "<br>";

echo $_SERVER['HTTP_REFERER'] . "<br>";
?>