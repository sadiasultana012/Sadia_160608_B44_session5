<?php

/*
echo "how are \"you\" ";

echo "<br>";

*/

$myStr = addslashes('Hello "37" Hello "37" Hello "37" Hello "37" ');

echo $myStr ."<br>" ;

echo "Hello \"37\" Hello \"37\" Hello \"37\" Hello \"37\" "."<br>";
$mystr = addslashes('Hello \37\ Hello "37" Hello \'37\' ');

echo $myStr ;
echo "<br>";


//explode here

$myStr = "Hello World! How is life?". "<br>";

$myArr   = explode(" ", $myStr);

print_r($myArr);

echo "<br>";

// implode with different chars
$myStr =   implode("*", $myArr);

echo $myStr. "<br>";

$myStr =   implode(" ", $myArr);

echo $myStr. "<br>";

$myStr =   implode(" * ", $myArr);

echo $myStr. "<br>";

//html entities

$myStr = ' <br> means line break';

$myStr = htmlentities($myStr);

echo $myStr. "<br>";

// trim

$myStr = "        Hello World        ";

echo $myStr. "<br>";

echo trim($myStr). "<br>";

//ltrim

echo ltrim($myStr). "<br>";

//rtrim

echo rtrim($myStr). "<br>";

//nl2br

echo "\n\n\n\n\n\nHi There!";

$myStr = "\n\n\n\n\nHi There!";

echo nl2br($myStr) . "<br>" . "<br>";

//htr_pad

$mainStr = " Hello World ";

$padStr = str_pad($mainStr,50, "*");

echo $padStr. "<br>";


$padStr = str_pad($mainStr,50, $mainStr);

echo $padStr. "<br>";

//str_repeat
$padStr = str_repeat($mainStr,5);

echo $padStr. "<br>" ;

//str_replace

echo "<br>". "<br>";

$mainStr = " Hello World ";

$replacedStr = str_replace("o", "O", $mainStr);

echo $replacedStr . "<br>";

echo "<br>". "<br>";

//str_split

$mainStr = " Hello World ";

$myArr = str_split($mainStr);

print_r($myArr);

echo "<br>";

//strlen

echo strlen($mainStr). "<br>";

//strtolower

echo "<br>". "<br>";

$mainStr = " Hello World ";

echo strtolower($mainStr);

//strtoupper


echo "<br>". "<br>";

$mainStr = " Hello World ";

echo strtoupper($mainStr);

//substr-compare

echo "<br>". "<br>";

$mainStr = "Hello World";

$subStr = "World";

echo substr_compare($mainStr, $subStr, 6) . "<br>" ;

//substr_count

$mainStr = "Hello World Hello World Hello World Hello World";

$subStr = "World";

echo substr_count($mainStr, $subStr). "<br>" ;

//substr_replace

$mainStr = "Hello World Hello World Hello World Hello World";

$subStr = "Hi";

echo substr_replace($mainStr, $subStr,10) . "<br>";

//ucfirst & ucwords

echo "<br>";

$mainStr = "hello world" . "<br>";

echo ucfirst($mainStr) . "<br>";

echo "<br>";

echo ucwords($mainStr) . "<br>";

echo "<br>";

























?>