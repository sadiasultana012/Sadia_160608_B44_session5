<?php

// boolean example started from here
$decision = true;
// if(expression == true) (do this)

if ($decision) echo "The decision is true <br>";

$decision = false;
// if(expression == true) (do this)
if ($decision) {
    echo "The decision is false";
}

// boolean example ended here

//integer example started from here

$value = 100;

echo $value;
echo "<br>";




//float/double example started from here

$value = 35.86;

echo $value . "<br>";


//float/double example ended here

//string example started from here

$var = 100;

$string1 = "this is double quoted string $var <br>";

$string2 = 'this is single quoted string $var <br>';

echo $string1.$string2;

/*
// trying heredoc

$trystring1 = <<<EOD

line 1 $var <br>
line 2 <br>
line 3
EOD;

echo $trystring1."<br>";

//ended heredoc

//trying nowdoc

$trystring2 = <<<'EOD'

line 1 $var
line 2
line 3
EOD;

echo $trystring2."<br>";

//ending nowdoc
*/

$heredocString = <<<bitm
  this is a heredoc example line 1 $var <br>
  this is a heredoc example line 2 $var <br>

bitm;


$nowdocString=<<<'bitm'
  this is a nowdoc example line 1 $var <br>
  this is a nowdoc example line 2 $var <br>


bitm;

echo $heredocString. "<br><br>"  .$nowdocString;
echo "<br><br>";

//string example ended here


// array starts

$arr = array(1,2,3,4,5,6,7,8,9,10);

print_r($arr);

echo "<br>";

$arr = array("BMW", "TOYOTA", "NISSAN", "PORSCHE", "FORD", "FERRAREE");
var_dump($arr);
print_r($arr);

echo "<br>";


$ageArray = array("Arif"=>30, "Moynar Ma"=>45, "Rahim"=>25);
print_r($ageArray);

echo "<br>";

echo "The age of Moynar Ma is " . $ageArray["Moynar Ma"];

echo "<br>";



?>


